FROM node:13.12
LABEL maintainer="Digital Dreamers"

RUN mkdir -p /app && chown -R node:node /app
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH

USER node
COPY --chown=node:node package.json package-lock*.json ./
RUN npm install --silent
